package com.getjavajob.training.socialnetwork.common;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.util.Objects;

import static java.util.Objects.hash;

@Entity
@Table(name = "friendship_tbl")
public class Friendship {

    @EmbeddedId
    private FriendshipId id;
    @MapsId("accountOneId")
    @ManyToOne
    @JoinColumn(name = "account_one_id")
    private Account accountFromId;
    @MapsId("accountTwoId")
    @ManyToOne
    @JoinColumn(name = "account_two_id")
    private Account accountToId;
    private boolean response;
    @ManyToOne
    @JoinColumn(name = "action_account")
    private Account actionAccount;

    public Friendship() {
    }

    public Friendship(Account accountFromId, Account accountToId) {
        this(accountFromId, accountToId, false, accountFromId);
    }

    public Friendship(Account accountFromId, Account accountToId, boolean response, Account actionAccount) {
        if (accountFromId.getId() < accountToId.getId()) {
            this.accountFromId = accountFromId;
            this.accountToId = accountToId;
        } else {
            this.accountFromId = accountToId;
            this.accountToId = accountFromId;
        }
        this.response = response;
        this.actionAccount = actionAccount;
        this.id = new FriendshipId(accountFromId.getId(), accountToId.getId());
    }

    public FriendshipId getFriendshipId() {
        return id;
    }

    public void setFriendshipId(FriendshipId id) {
        this.id = id;
    }

    public Account getAccountFromId() {
        return accountFromId;
    }

    public void setAccountFromId(Account accountFromId) {
        this.accountFromId = accountFromId;
    }

    public Account getAccountToId() {
        return accountToId;
    }

    public void setAccountToId(Account accountToId) {
        this.accountToId = accountToId;
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public Account getActionAccount() {
        return actionAccount;
    }

    public void setActionAccount(Account actionAccount) {
        this.actionAccount = actionAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Friendship that = (Friendship) o;
        return response == that.response &&
                Objects.equals(accountFromId, that.accountFromId) &&
                Objects.equals(accountToId, that.accountToId) &&
                Objects.equals(actionAccount, that.actionAccount);
    }

    @Override
    public int hashCode() {
        return hash(accountFromId, accountToId, response, actionAccount);
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "id=" + id +
                ", accountFromId=" + accountFromId +
                ", accountToId=" + accountToId +
                ", response=" + response +
                ", actionAccount=" + actionAccount +
                '}';
    }

}
