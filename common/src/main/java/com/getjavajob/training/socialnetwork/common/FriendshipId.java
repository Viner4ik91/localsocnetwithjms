package com.getjavajob.training.socialnetwork.common;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class FriendshipId implements Serializable {

    @Column(name = "account_one_id")
    private long accountOneId;
    @Column(name = "account_two_id")
    private long accountTwoId;

    public FriendshipId(long accountOneId, long accountTwoId) {
        if (accountOneId < accountTwoId) {
            this.accountOneId = accountOneId;
            this.accountTwoId = accountTwoId;
        } else {
            this.accountOneId = accountTwoId;
            this.accountTwoId = accountOneId;
        }
    }

    public FriendshipId() {
    }

    public long getAccountOneId() {
        return accountOneId;
    }

    public void setAccountOneId(int accountOneId) {
        this.accountOneId = accountOneId;
    }

    public long getAccountTwoId() {
        return accountTwoId;
    }

    public void setAccountTwoId(int accountTwoId) {
        this.accountTwoId = accountTwoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FriendshipId that = (FriendshipId) o;
        return accountOneId == that.accountOneId &&
                accountTwoId == that.accountTwoId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountOneId, accountTwoId);
    }

    @Override
    public String toString() {
        return "FriendshipId{" +
                "accountOneId=" + accountOneId +
                ", accountTwoId=" + accountTwoId +
                '}';
    }

}
