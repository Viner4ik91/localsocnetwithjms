package com.getjavajob.training.socialnetwork.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "group_tbl")
public class Group implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @OneToOne
    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    private Account creator;
    @Column(name = "group_name")
    private String groupName;
    private String description;
    @Column(name = "creation_date")
    private LocalDate creationDate;
    private byte[] image;

    public Group(Account creator, String groupName, String description) {
        this.creator = creator;
        this.groupName = groupName;
        this.description = description;
    }

    public Group(long id, Account creator, String groupName, String description) {
        this(creator, groupName, description);
        this.id = id;
    }

    public Group() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Account getCreator() {
        return creator;
    }

    public void setCreator(Account creator) {
        this.creator = creator;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Group group = (Group) o;
        return id == group.id &&
                Objects.equals(creator, group.creator) &&
                Objects.equals(groupName, group.groupName) &&
                Objects.equals(description, group.description) &&
                Objects.equals(creationDate, group.creationDate) &&
                Arrays.equals(image, group.image);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, creator, groupName, description, creationDate);
        result = 31 * result + Arrays.hashCode(image);
        return result;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", creator=" + creator +
                ", groupName='" + groupName + '\'' +
                ", description='" + description + '\'' +
                ", creationDate=" + creationDate +
                ", image=" + Arrays.toString(image) +
                '}';
    }

}