package com.getjavajob.training.socialnetwork.common;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class GroupMembershipId implements Serializable {

    @Column(name = "group_id")
    private long groupId;
    @Column(name = "account_id")
    private long accountId;

    public GroupMembershipId() {
    }

    public GroupMembershipId(long groupId, long accountId) {
        this.groupId = groupId;
        this.accountId = accountId;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GroupMembershipId that = (GroupMembershipId) o;
        return groupId == that.groupId &&
                accountId == that.accountId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupId, accountId);
    }

    @Override
    public String toString() {
        return "GroupMembershipId{" +
                "groupId=" + groupId +
                ", accountId=" + accountId +
                '}';
    }

}
