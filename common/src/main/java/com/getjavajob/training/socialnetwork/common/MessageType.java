package com.getjavajob.training.socialnetwork.common;

public enum MessageType {
    ACCOUNT, GROUP, PERSONAL
}
