package com.getjavajob.training.socialnetwork.common.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class CommonUtils {

    public static String getEncodePassword(String password) {
        return new BCryptPasswordEncoder().encode(password);
    }

}
