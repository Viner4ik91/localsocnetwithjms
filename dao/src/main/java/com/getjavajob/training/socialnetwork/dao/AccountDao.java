package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountDao extends CrudRepository<Account, Long> {

    @Query("SELECT a FROM Account a LEFT JOIN FETCH a.phones WHERE a.email=:email")
    Account getByEmail(@Param("email") String email);

    @Override
    @Query("SELECT a FROM Account a LEFT JOIN FETCH a.phones WHERE a.id=:id")
    Optional<Account> findById(@Param("id") Long id);

    @Query("SELECT DISTINCT a FROM Account a WHERE LOWER(a.name) LIKE" +
            " CONCAT('%', LOWER(:value), '%') OR LOWER(a.surname) LIKE CONCAT('%', LOWER(:value), '%')")
    List<Account> searchAccounts(@Param("value") String value);

    @Query("SELECT DISTINCT a FROM Account a WHERE LOWER(a.name) LIKE" +
            " CONCAT('%', LOWER(:value), '%') OR LOWER(a.surname) LIKE CONCAT('%', LOWER(:value), '%')")
    List<Account> searchByPage(@Param("value") String value, Pageable pageable);

    @Query("SELECT DISTINCT COUNT(a) FROM Account a WHERE LOWER(a.name) LIKE CONCAT('%', LOWER(:value), '%') " +
            "OR LOWER(a.surname) LIKE CONCAT('%', LOWER(:value), '%')")
    int getAccountQty(@Param("value") String value);

}
