package com.getjavajob.training.socialnetwork.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

public class ConnectionPool {

    private static final String PROPERTY_FILE = "configDao.properties";
    private static final int POOL_SIZE = 10;
    private static final int TIMEOUT_CNT = 2;

    private BlockingQueue<Connection> connections = new ArrayBlockingQueue<>(POOL_SIZE);

    public ConnectionPool() {
        try {
            Properties props = new Properties();
            props.load(this.getClass().getClassLoader().getResourceAsStream(PROPERTY_FILE));
            String driver = props.getProperty("database.driver");
            Class.forName(driver);
            String url = props.getProperty("database.url");
            String user = props.getProperty("database.user");
            String password = props.getProperty("database.password");

            for (int i = 0; i < POOL_SIZE; i++) {
                Connection connection = DriverManager.getConnection(url, user, password);
                connection.setAutoCommit(false);
                //TODO this and conventions
                connections.add(connection);
            }
            System.out.println("Connection done");
        } catch (SQLException | IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        try {
            //TODO MAGIC numbers
            if (connections.peek() != null && connections.peek().isValid(TIMEOUT_CNT)) {
                return connections.poll(TIMEOUT_CNT, TimeUnit.SECONDS);
            } else {
                requireNonNull(connections.poll(TIMEOUT_CNT, TimeUnit.SECONDS)).close();
                return getConnection();
            }
        } catch (InterruptedException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void close(Connection connection) {
        try {
            if (connection.isValid(TIMEOUT_CNT)) {
                connections.add(connection);
            } else {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
