package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Friendship;
import com.getjavajob.training.socialnetwork.common.FriendshipId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendshipDao extends CrudRepository<Friendship, FriendshipId> {

    @Query("SELECT r FROM Friendship r WHERE r.accountFromId.id = :id OR r.accountToId.id = :id")
    List<Friendship> getAllRelations(@Param("id") long id);

}
