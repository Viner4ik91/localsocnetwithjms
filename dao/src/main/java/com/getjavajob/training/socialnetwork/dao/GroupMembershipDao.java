package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.GroupMembership;
import com.getjavajob.training.socialnetwork.common.GroupMembershipId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupMembershipDao extends CrudRepository<GroupMembership, GroupMembershipId> {

    @Query("SELECT gm FROM GroupMembership gm WHERE gm.member.id = :id")
    List<GroupMembership> getAllGroups(@Param("id") long id);

    @Query("SELECT gm FROM GroupMembership gm WHERE gm.group.id = :groupId")
    List<GroupMembership> getAllAccounts(@Param("groupId") long id);

}
