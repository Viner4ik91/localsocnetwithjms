package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Message;
import com.getjavajob.training.socialnetwork.common.MessageType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageDao extends CrudRepository<Message, Long> {

    void deleteAllBySenderOrRecipientId(@Param("sender") Account sender, @Param("recipientId") long recipientId);

    @Query("SELECT m FROM Message m WHERE ((m.sender.id = :firstId OR m.recipientId = :firstId) AND m.type = 2) " +
            "AND ((m.sender.id = :secondId OR m.recipientId = :secondId) AND m.type = 2)")
    List<Message> getChat(@Param("firstId") long firstId, @Param("secondId") long secondId);

    @Query("SELECT DISTINCT (CASE m.sender.id WHEN :id THEN m.recipientId ELSE m.sender.id END)" +
            " FROM Message m WHERE (m.sender.id = :id OR m.recipientId = :id) AND m.type = 2")
    List<Long> getInterlocutors(@Param("id") long id);

    List<Message> findByRecipientIdAndTypeOrderByCreationDateDesc(@Param("recipientId") long recipientId, @Param("type") MessageType type);

}