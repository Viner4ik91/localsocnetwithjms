package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-test-context.xml"})
public class AccountDaoTest extends DaoTest {

    @Autowired
    private AccountDao dao;

    private List<Account> accounts = createAccounts();

    @Transactional
    @Test
    public void createAccountTest() {
        Account account = new Account("Pupkin", "Vasiliy", "pup@mail.ru", "1234");
        accounts.add(dao.save(account));
        assertEquals(accounts.get(3), dao.findById(4L).orElse(null));
    }

    @Transactional
    @Test
    public void getByIdTest() {
        Account actual = dao.findById(1L).orElse(null);
        assertEquals(accounts.get(0), actual);
    }

    @Transactional
    @Test
    public void updateAccountTest() {
        Account account = new Account(1, "A", "BBB", "vin@mail.ru", "1234");
        dao.save(account);
        assertEquals(account, dao.findById(1L).orElse(null));
    }

    @Transactional
    @Test
    public void deleteByIdTest() {
        dao.deleteById(1L);
        assertNull(dao.findById(1L).orElse(null));
    }

    @Transactional
    @Test
    public void getAllTest() {
        assertEquals(accounts, dao.findAll());
    }

    @Transactional
    @Test
    public void getByEmailTest() {
        assertEquals(accounts.get(0), dao.getByEmail("vin@mail.ru"));
    }

    @Transactional
    @Test
    public void searchAccountsTest() {
        assertEquals(accounts.get(0), dao.searchAccounts("br").get(0));
    }

}