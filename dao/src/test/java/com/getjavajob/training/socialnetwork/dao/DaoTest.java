package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Friendship;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.GroupMembership;
import com.getjavajob.training.socialnetwork.common.Message;
import com.getjavajob.training.socialnetwork.common.MessageType;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Sql(value = "classpath:CreateDatabasesTest.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:DropDatabasesTest.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class DaoTest {

    protected static List<Account> createAccounts() {
        Account account1 = new Account(1, "Ibragimov", "Viner", "vin@mail.ru", "1234");
        account1.setRegistrationDate(LocalDate.now());
        Account account2 = new Account(2, "Ivanov", "Ivan", "ivn@mail.ru", "1234");
        account2.setRegistrationDate(LocalDate.now());
        Account account3 = new Account(3, "A", "B", "B@mail.ru", "1234");
        account3.setRegistrationDate(LocalDate.now());
        List<Account> accounts = new ArrayList<>();
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        return accounts;
    }

    protected static List<Group> createGroups() {
        Account creator = new Account(1, "Ibragimov", "Viner", "vin@mail.ru", "1234");
        creator.setRegistrationDate(LocalDate.now());
        Group group1 = new Group(1, creator, "groupOne", "super");
        group1.setCreationDate(LocalDate.now());
        Group group2 = new Group(2, creator, "groupTwo", "cool");
        group2.setCreationDate(LocalDate.now());
        List<Group> groups = new ArrayList<>();
        groups.add(group1);
        groups.add(group2);
        return groups;
    }

    protected static List<Friendship> createFriendships() {
        List<Account> accounts = createAccounts();
        Friendship friendshipOne = new Friendship(accounts.get(0), accounts.get(1), true, accounts.get(1));
        Friendship friendshipTwo = new Friendship(accounts.get(0), accounts.get(2));
        Friendship friendshipThree = new Friendship(accounts.get(1), accounts.get(2));
        List<Friendship> friendships = new ArrayList<>();
        friendships.add(friendshipOne);
        friendships.add(friendshipTwo);
        friendships.add(friendshipThree);
        return friendships;
    }

    protected static List<GroupMembership> createMemberships() {
        List<Account> accounts = createAccounts();
        List<Group> groups = createGroups();
        GroupMembership groupAdmin = new GroupMembership(groups.get(0), accounts.get(1), true, true);
        GroupMembership groupMember = new GroupMembership(groups.get(0), accounts.get(0), true, false);
        GroupMembership groupRequest = new GroupMembership(groups.get(1), accounts.get(1));
        List<GroupMembership> memberships = new ArrayList<>();
        memberships.add(groupAdmin);
        memberships.add(groupMember);
        memberships.add(groupRequest);
        return memberships;
    }

    protected static List<Message> createMessages() {
        List<Account> accounts = createAccounts();
        List<Group> groups = createGroups();
        Message wallMessageAccount = new Message(accounts.get(0), accounts.get(1).getId(), "1", MessageType.ACCOUNT);
        wallMessageAccount.setId(1);
        wallMessageAccount.setCreationDate(LocalDateTime.now());
        Message wallMessageGroup = new Message(accounts.get(0), groups.get(0).getId(), "2", MessageType.GROUP);
        wallMessageGroup.setId(2);
        wallMessageGroup.setCreationDate(LocalDateTime.now());
        Message personalMessage = new Message(accounts.get(0), accounts.get(1).getId(), "3", MessageType.PERSONAL);
        personalMessage.setId(3);
        personalMessage.setCreationDate(LocalDateTime.now());
        List<Message> messages = new ArrayList<>();
        messages.add(wallMessageAccount);
        messages.add(wallMessageGroup);
        messages.add(personalMessage);
        return messages;
    }

}
