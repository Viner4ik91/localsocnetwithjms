package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Friendship;
import com.getjavajob.training.socialnetwork.common.FriendshipId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-test-context.xml"})
public class FriendshipDaoTest extends DaoTest {

    @Autowired
    private FriendshipDao dao;
    @Autowired
    private AccountDao accountDao;

    private List<Friendship> friendships = createFriendships();

    @Transactional
    @Test
    public void createRelationTest() {
        Account accountOne = new Account("asdasd", "fsgd", "olkd@mail.ru", "1234");
        accountOne.setRegistrationDate(LocalDate.now());
        accountDao.save(accountOne);
        Account accountTwo = new Account("hjlg", "rff", "iutu@mail.ru", "1234");
        accountTwo.setRegistrationDate(LocalDate.now());
        accountDao.save(accountTwo);
        Friendship friendship = new Friendship(accountOne, accountTwo, false, accountTwo);
        friendships.add(dao.save(friendship));
        assertEquals(friendships.get(3), dao.findById(new FriendshipId(4, 5)).orElse(null));
    }

    @Transactional
    @Test
    public void getTest() {
        Friendship actual = dao.findById(new FriendshipId(1, 2)).orElse(null);
        assertEquals(friendships.get(0), actual);
    }

    @Transactional
    @Test
    public void getNullTest() {
        Friendship actual = dao.findById(new FriendshipId(5, 6)).orElse(null);
        assertNull(actual);
    }

    @Transactional
    @Test
    public void updateRelationTest() {
        Friendship friendship = dao.findById(new FriendshipId(1, 3)).orElse(null);
        Objects.requireNonNull(friendship).setResponse(true);
        dao.save(friendship);
        assertEquals(friendship, dao.findById(new FriendshipId(1, 3)).orElse(null));
    }

    @Transactional
    @Test
    public void deleteTest() {
        dao.delete(dao.findById(new FriendshipId(1, 2)).orElse(null));
        assertNull(dao.findById(new FriendshipId(1, 2)).orElse(null));
    }

    @Transactional
    @Test
    public void getAllTest() {
        assertEquals(friendships, dao.findAll());
    }

    @Transactional
    @Test
    public void getAllRelationsTest() {
        List<Friendship> actual = friendships;
        actual.remove(2);
        assertEquals(dao.getAllRelations(1), actual);
    }

}