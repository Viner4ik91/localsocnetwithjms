DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS group_tbl;

CREATE TABLE IF NOT EXISTS account (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
surname VARCHAR(20) NOT NULL,
name VARCHAR(20) NOT NULL,
middlename VARCHAR(20),
birthday DATE,
person_phone VARCHAR(11),
work_phone VARCHAR(11),
home_address VARCHAR(45),
work_address VARCHAR(45),
email VARCHAR(45),
icq VARCHAR(45),
skype VARCHAR(45),
add_info VARCHAR(45)
);

CREATE TABLE IF NOT EXISTS group_tbl (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
group_name VARCHAR(20) NOT NULL
);

INSERT INTO account(
surname,
name,
email
)
VALUES (
'Ibragimov',
'Viner',
'vin@mail.ru'
);

INSERT INTO account(
surname,
name,
email
)
VALUES (
'Ivanov',
'Ivan',
'ivn@mail.ru'
);

INSERT INTO account(
surname,
name,
email
)
VALUES (
'A',
'B',
'C@mail.ru'
);

INSERT INTO group_tbl(
group_name
)
VALUES (
'groupOne'
);

INSERT INTO group_tbl(
group_name
)
VALUES (
'groupTwo'
);