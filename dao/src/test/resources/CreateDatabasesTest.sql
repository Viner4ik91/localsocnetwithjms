DROP TABLE IF EXISTS account_tbl;
DROP TABLE IF EXISTS group_tbl;
DROP TABLE IF EXISTS phone_tbl;
DROP TABLE IF EXISTS friendship_tbl;
DROP TABLE IF EXISTS group_membership_tbl;
DROP TABLE IF EXISTS message_tbl;

CREATE TABLE IF NOT EXISTS account_tbl (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
surname VARCHAR(20) NOT NULL,
name VARCHAR(20) NOT NULL,
middlename VARCHAR(20),
birthday DATE,
home_address VARCHAR(45),
work_address VARCHAR(45),
email VARCHAR(45) NOT NULL UNIQUE,
icq VARCHAR(45),
skype VARCHAR(45),
add_info VARCHAR(45),
password VARCHAR(65),
admin BOOLEAN NOT NULL DEFAULT FALSE,
image BLOB,
registration_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS group_tbl (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
creator_id INT NOT NULL,
group_name VARCHAR(20) NOT NULL,
description VARCHAR(20) NOT NULL,
image BLOB,
creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS phone_tbl (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
account_id INT NOT NULL,
type VARCHAR(20) NOT NULL,
number VARCHAR(20) NOT NULL,
CONSTRAINT acc_id_for FOREIGN KEY (account_id) REFERENCES account_tbl(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS friendship_tbl (
account_one_id INT NOT NULL,
account_two_id INT NOT NULL,
response BOOLEAN NOT NULL DEFAULT FALSE,
action_account INT NOT NULL,
CONSTRAINT friends_pr PRIMARY KEY (account_one_id, account_two_id),
CONSTRAINT account_one_id_for FOREIGN KEY (account_one_id) REFERENCES account_tbl(id) ON DELETE CASCADE,
CONSTRAINT account_two_id_for FOREIGN KEY (account_two_id) REFERENCES account_tbl(id) ON DELETE CASCADE,
CONSTRAINT action_account_for FOREIGN KEY (action_account) REFERENCES account_tbl(id)
);

CREATE TABLE IF NOT EXISTS group_membership_tbl (
group_id INT NOT NULL,
account_id INT NOT NULL,
response BOOLEAN NOT NULL DEFAULT FALSE,
admin BOOLEAN NOT NULL DEFAULT FALSE,
CONSTRAINT members_pr PRIMARY KEY (group_id, account_id),
CONSTRAINT account_id_for FOREIGN KEY (account_id) REFERENCES account_tbl(id) ON DELETE CASCADE,
CONSTRAINT group_id_for FOREIGN KEY (group_id) REFERENCES group_tbl(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS message_tbl (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
sender_id INT NOT NULL,
recipient_id INT NOT NULL,
msg_txt TEXT NOT NULL,
creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
type TINYINT NOT NULL
);

INSERT INTO account_tbl(surname, name, email, password) VALUES('Ibragimov', 'Viner', 'vin@mail.ru', '1234');
INSERT INTO account_tbl(surname, name, email, password) VALUES('Ivanov', 'Ivan', 'ivn@mail.ru', '1234');
INSERT INTO account_tbl(surname, name, email, password) VALUES('A', 'B', 'B@mail.ru', '1234');

INSERT INTO group_tbl(creator_id, group_name, description) VALUES('1', 'groupOne', 'super');
INSERT INTO group_tbl(creator_id, group_name, description) VALUES('1', 'groupTwo', 'cool');

INSERT INTO friendship_tbl VALUES('1', '2', '1', '2');
INSERT INTO friendship_tbl VALUES('1', '3', '0', '1');
INSERT INTO friendship_tbl VALUES('2', '3', '0', '2');

INSERT INTO group_membership_tbl VALUES('1', '2', '1', '1');
INSERT INTO group_membership_tbl VALUES('1', '1', '1', '0');
INSERT INTO group_membership_tbl VALUES('2', '2', '0', '0');

INSERT INTO message_tbl(sender_id, recipient_id, msg_txt, type) VALUES('1', '2', '1','0');
INSERT INTO message_tbl(sender_id, recipient_id, msg_txt, type) VALUES('1', '1', '2','1');
INSERT INTO message_tbl(sender_id, recipient_id, msg_txt, type) VALUES('1', '2', '3','2');




