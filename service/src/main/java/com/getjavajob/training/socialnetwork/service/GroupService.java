package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.GroupMembership;
import com.getjavajob.training.socialnetwork.common.GroupMembershipId;
import com.getjavajob.training.socialnetwork.common.Message;
import com.getjavajob.training.socialnetwork.common.MessageType;
import com.getjavajob.training.socialnetwork.dao.GroupDao;
import com.getjavajob.training.socialnetwork.dao.GroupMembershipDao;
import com.getjavajob.training.socialnetwork.dao.MessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@Transactional
public class GroupService {

    @Autowired
    private GroupDao dao;
    @Autowired
    private GroupMembershipDao membershipDao;
    @Autowired
    private MessageDao messageDao;

    public List<Group> searchGroups(String value) {
        return dao.searchGroups(value);
    }

    public List<Group> searchByPageGroups(String value, int page, int maxResult) {
        return dao.searchByPage(value, PageRequest.of(page, maxResult));
    }

    public Group createGroup(Group group) {
        Group newGroup = dao.save(group);
        GroupMembership membership = new GroupMembership(newGroup, newGroup.getCreator(), true, true);
        group.setCreationDate(LocalDate.now());
        membershipDao.save(membership);
        return newGroup;
    }

    public void updateGroup(Group group) {
        dao.save(group);
    }

    public void deleteById(long id) {
        dao.deleteById(id);
    }

    public Group getById(long id) {
        return dao.findById(id).orElse(null);
    }

    public long getGroupQty(String search) {
        return dao.getGroupQty(search);
    }

    //    Membership
    public void addGroupMember(Account account, Group group) {
        membershipDao.save(new GroupMembership(group, account));
    }

    public void acceptGroupMember(long accountId, long groupId) {
        membershipDao.findById(new GroupMembershipId(accountId, groupId)).
                ifPresent(groupMembership -> groupMembership.setResponse(true));
    }

    public void makeModerator(long accountId, long groupId) {
        membershipDao.findById(new GroupMembershipId(accountId, groupId)).
                ifPresent(groupMembership -> groupMembership.setAdmin(true));
    }

    public void deleteGroupMember(long accountId, long groupId) {
        membershipDao.findById(new GroupMembershipId(accountId, groupId)).
                ifPresent(groupMembership -> membershipDao.delete(groupMembership));
    }

    public Map<String, List<Group>> getAccountGroups(long id) {
        List<Group> groups = new ArrayList<>();
        List<Group> requests = new ArrayList<>();
        for (GroupMembership membership : membershipDao.getAllGroups(id)) {
            if (membership.isResponse()) {
                groups.add(membership.getGroup());
            } else {
                requests.add(membership.getGroup());
            }
        }
        Map<String, List<Group>> allGroups = new HashMap<>();
        allGroups.put("groups", groups);
        allGroups.put("requests", requests);
        return allGroups;
    }

    public Map<String, List<Account>> getGroupMembers(long groupId) {
        List<Account> members = new ArrayList<>();
        List<Account> requests = new ArrayList<>();
        List<Account> moderators = new ArrayList<>();
        for (GroupMembership membership : membershipDao.getAllAccounts(groupId)) {
            if (membership.isResponse() && membership.isAdmin()) {
                moderators.add(membership.getMember());
            } else if (membership.isResponse()) {
                members.add(membership.getMember());
            } else {
                requests.add(membership.getMember());
            }
        }
        Map<String, List<Account>> allAccounts = new HashMap<>();
        allAccounts.put("members", members);
        allAccounts.put("requests", requests);
        allAccounts.put("moderators", moderators);
        return allAccounts;
    }

    public boolean isModerator(long id, long groupId) {
        if (!isMember(id, groupId)) {
            return false;
        }
        return Objects.requireNonNull(membershipDao.findById(new GroupMembershipId(groupId, id)).orElse(null)).isAdmin();
    }

    public boolean isMember(long id, long groupId) {
        return membershipDao.existsById(new GroupMembershipId(groupId, id));
    }

    //    Message
    public void sendGroupPost(Account sender, long recipientId, String text) {
        Message message = new Message(sender, recipientId, text, MessageType.GROUP);
        message.setCreationDate(LocalDateTime.now());
        messageDao.save(message);
    }

    public void deleteGroupPost(long id) {
        messageDao.deleteById(id);
    }

    public List<Message> getAllGroupPosts(long groupId) {
        return messageDao.findByRecipientIdAndTypeOrderByCreationDateDesc(groupId, MessageType.GROUP);
    }

}
