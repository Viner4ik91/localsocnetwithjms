package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Friendship;
import com.getjavajob.training.socialnetwork.common.FriendshipId;
import com.getjavajob.training.socialnetwork.dao.AccountDao;
import com.getjavajob.training.socialnetwork.dao.FriendshipDao;
import com.getjavajob.training.socialnetwork.dao.MessageDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountDao accountDao;
    @Mock
    private FriendshipDao friendshipDao;
    @Mock
    private MessageDao messageDao;
    @InjectMocks
    private AccountService accountService;

    @Test
    public void createAccountTest() {
        Account account = new Account(1, "Ibragimov", "Viner", "vin@mail.ru", "1234");
        when(accountDao.save(account)).thenReturn(account);
        Account actual = accountService.createAccount(account);
        assertEquals(account, actual);
        verify(accountDao, times(1)).save(account);
    }

    @Test
    public void updateAccountTest() {
        Account account = new Account(1, "dsgds", "Viner", "vin@mail.ru", "1234");
        accountService.updateAccount(account);
        verify(accountDao).save(account);
    }

    @Test
    public void deleteAccountTest() {
        Account account = new Account(1, "dsgds", "Viner", "vin@mail.ru", "1234");
        accountService.deleteAccount(account);
        verify(messageDao).deleteAllBySenderOrRecipientId(account, account.getId());
        verify(accountDao).delete(account);
    }

    @Test
    public void getByIdTest() {
        Account account = new Account(1, "dsgds", "Viner", "vin@mail.ru", "1234");
        when(accountDao.findById(1L)).thenReturn(java.util.Optional.of(account));
        Account actual = accountService.getById(1);
        assertEquals(account, actual);
        verify(accountDao, times(1)).findById(1L);
    }

    @Test
    public void getByEmailTest() {
        Account account = new Account(1, "dsgds", "Viner", "vin@mail.ru", "1234");
        when(accountDao.getByEmail("vin@mail.ru")).thenReturn(account);
        Account actual = accountService.getByEmail("vin@mail.ru");
        assertEquals(account, actual);
        verify(accountDao, times(1)).getByEmail("vin@mail.ru");
    }

    @Test
    public void searchAccountsTest() {
        List<Account> expected = new ArrayList<>();
        expected.add(new Account(1, "dsgds", "Viner", "vin@mail.ru", "1234"));
        expected.add(new Account(2, "VinDiesel", "Viner", "vin@mail.ru", "1234"));
        when(accountDao.searchAccounts("Vin")).thenReturn(expected);
        List<Account> actual = accountService.searchAccounts("Vin");
        assertEquals(expected, actual);
        verify(accountDao, times(1)).searchAccounts("Vin");
    }

    @Test
    public void checkFriendshipTrueTest() {
        Account accountOne = new Account("asdasd", "fsgd", "olkd@mail.ru", "1234");
        accountOne.setRegistrationDate(LocalDate.now());
        Account accountTwo = new Account("hjlg", "rff", "iutu@mail.ru", "1234");
        accountTwo.setRegistrationDate(LocalDate.now());
        Friendship friendship = new Friendship(accountOne, accountTwo, true, accountTwo);
        when(friendshipDao.findById(new FriendshipId(1, 2))).thenReturn(java.util.Optional.of(friendship));
        assertTrue(accountService.checkFriendship(1, 2));
        verify(friendshipDao, times(1)).findById(new FriendshipId(1, 2));
    }

}