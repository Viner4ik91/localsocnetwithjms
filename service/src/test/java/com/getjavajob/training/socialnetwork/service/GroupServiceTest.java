package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.GroupMembership;
import com.getjavajob.training.socialnetwork.dao.GroupDao;
import com.getjavajob.training.socialnetwork.dao.GroupMembershipDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest {

    @Mock
    private GroupDao groupDao;
    @Mock
    private GroupMembershipDao membershipDao;
    @InjectMocks
    private GroupService groupService;

    @Test
    public void createGroupTest() {
        Account account = new Account(1, "Ibragimov", "Viner", "vin@mail.ru", "1234");
        Group group = new Group(1, account, "one", "super");
        when(groupDao.save(group)).thenReturn(group);
        Group actual = groupService.createGroup(group);
        assertEquals(group, actual);
        verify(groupDao, times(1)).save(group);
    }

    @Test
    public void getSearchGroupsTest() {
        List<Group> groups = new ArrayList<>();
        Account account1 = new Account(1, "Ibragimov", "Viner", "vin@mail.ru", "1234");
        Account account2 = new Account(2, "Ivanov", "Ivan", "ivn@mail.ru", "1234");
        groups.add(new Group(1, account1, "one", "super"));
        groups.add(new Group(2, account2, "two", "cool"));
        groups.add(new Group(3, account1, "oneDouble", "best"));
        List<Group> expected = new ArrayList<>();
        expected.add(groups.get(0));
        expected.add(groups.get(2));
        when(groupDao.searchGroups("one")).thenReturn(expected);
        List<Group> actual = groupService.searchGroups("one");
        assertEquals(expected, actual);
        verify(groupDao, times(1)).searchGroups("one");
    }

    @Test
    public void updateGroupTest() {
        Account account = new Account(1, "Ibragimov", "Viner", "vin@mail.ru", "1234");
        Group group = new Group(1, account, "one", "super");
        groupService.updateGroup(group);
        verify(groupDao).save(group);
    }

    @Test
    public void deleteByIdTest() {
        groupService.deleteById(1);
        verify(groupDao).deleteById(1L);
    }

    @Test
    public void getByIdTest() {
        Account account = new Account(1, "Ibragimov", "Viner", "vin@mail.ru", "1234");
        Group group = new Group(1, account, "one", "super");
        when(groupDao.findById(1L)).thenReturn(java.util.Optional.of(group));
        Group actual = groupService.getById(1);
        assertEquals(group, actual);
        verify(groupDao, times(1)).findById(1L);
    }

    @Test
    public void createMembershipTest() {
        Account account = new Account(1, "Ibragimov", "Viner", "vin@mail.ru", "1234");
        Group group = new Group(1, account, "one", "super");
        GroupMembership membership = new GroupMembership(group, account);
        when(membershipDao.save(membership)).thenReturn(membership);
        groupService.addGroupMember(account, group);
        verify(membershipDao, times(1)).save(membership);
    }

}
