package com.getjavajob.training.socialnetwork.ui;

import com.getjavajob.training.mailer.web.JmsMessage;
import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Phone;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.getjavajob.training.socialnetwork.Application.ORDER_QUEUE;
import static com.getjavajob.training.socialnetwork.common.utils.CommonUtils.getEncodePassword;
import static java.util.Objects.isNull;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.MediaType.APPLICATION_XML;

@Controller
public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private static final int MAX_IMAGE_SIZE = 65000;
    private static final int MIN_IMAGE_SIZE = 0;

    @Autowired
    private AccountService accountService;

    @Autowired
    private JmsTemplate jmsTemplate;

    @InitBinder
    protected void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public ModelAndView accountHome(@RequestParam(required = false, name = "id") long id,
                                    @SessionAttribute("account") Account sessionAccount) {
        ModelAndView modelAndView = new ModelAndView("accountView");
        long sessionId = sessionAccount.getId();
        if (sessionId == id) {
            logger.info("Loading home page of the session account with id='{}'", id);
            modelAndView.addObject(sessionAccount);
            modelAndView.addObject("wallMessages", accountService.getAccountPosts(sessionId));
        } else {
            logger.info("Loading home page of the account with id='{}'", id);
            Account account = accountService.getById(id);
            modelAndView.addObject("account", account);
            modelAndView.addObject("isFriend", accountService.checkFriendship(sessionId, id));
            modelAndView.addObject("isSubscribe", accountService.checkRequestToMe(sessionId, id));
            modelAndView.addObject("isMyRequest", accountService.checkRequestFromMe(sessionId, id));
            modelAndView.addObject("wallMessages", accountService.getAccountPosts(id));
        }
        return modelAndView;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(@RequestParam(value = "error", required = false) String error,
                        @RequestParam(value = "logout", required = false) String logout, Model model,
                        @SessionAttribute(required = false) Account account) {
        if (error != null) {
            model.addAttribute("error", "Incorrect username or password!");
        } else if (logout != null) {
            model.addAttribute("logout", "You've been logged out successfully.");
        } else {
            if (account != null) {
                return "redirect:/account?id=" + account.getId();
            }
        }
        return "loginView";
    }

    @RequestMapping(value = "/toXml", method = RequestMethod.GET)
    public void createAccountXml(@RequestParam long id, HttpServletResponse response) {
        XStream xstream = new XStream();
        xstream.processAnnotations(Account.class);
        response.addHeader(CONTENT_DISPOSITION, "attachment;filename=account.xml");
        response.setContentType(String.valueOf(APPLICATION_XML));
        Account account = accountService.getById(id);
        logger.info("Account with id {} converts account to xml", account.getId());
        try {
            response.getOutputStream().write(xstream.toXML(account).getBytes());
            response.flushBuffer();
        } catch (IOException e) {
            logger.error("Exception was thrown", e);
            response.setStatus(SC_BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/fromXml", method = RequestMethod.POST)
    public ModelAndView uploadXml(@RequestParam long id, @RequestParam("file") MultipartFile file,
                                  HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("updateAccountView");
        Account account = accountService.getById(id);
        if (isNull(file) || file.isEmpty()) {
            response.setStatus(SC_BAD_REQUEST);
            return modelAndView.addObject("account", account)
                    .addObject("error", "XML file is required");
        } else {
            XStream xstream = new XStream();
            xstream.processAnnotations(Account.class);
            logger.info("Account with id {} uploads account from xml", id);
            try {
                Account xmlAccount = (Account) xstream.fromXML(new String(file.getBytes()));
                xmlAccount.setPassword(account.getPassword());
                return modelAndView.addObject("account", xmlAccount);
            } catch (IOException | XStreamException e) {
                response.setStatus(SC_BAD_REQUEST);
                logger.error("Wrong format of XML", e);
                return modelAndView.addObject("account", account)
                        .addObject("error", "Wrong XML format");
            }
        }
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping("/deleteAccount")
    public String deleteAccount(@SessionAttribute("account") Account account, @RequestParam long id,
                                HttpSession httpSession) {
        logger.info("Account with id='{}' deleted", id);
        Account delAccount;
        if (account.isAdmin() && id != account.getId()) {
            delAccount = accountService.getById(id);
        } else {
            delAccount = account;
            httpSession.removeAttribute("account");
        }
        accountService.deleteAccount(delAccount);
        jmsTemplate.convertAndSend(ORDER_QUEUE, new JmsMessage(account.getEmail(), "Your account is deleted forever"));
        return "redirect:/login";
    }

    @RequestMapping(value = "/updateAccount", method = RequestMethod.GET)
    public ModelAndView showUpdate(@RequestParam("id") long id, @RequestParam(required = false) String error) {
        Account account = accountService.getById(id);
        ModelAndView modelAndView = new ModelAndView("updateAccountView");
        modelAndView.addObject("account", account).addObject("error", error);
        return modelAndView;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or #account.id == sessionAccount.id")
    @RequestMapping(value = "/updateAccount", method = RequestMethod.POST)
    public String doUpdate(@ModelAttribute Account account, HttpSession session,
                           @SessionAttribute("account") Account sessionAccount) {
        logger.info("Edit account with id='{}'", account.getId());
        removeNullPhones(account);
        long id = account.getId();
        account.setPassword(getEncodePassword(account.getPassword()));
        Account oldAccount = sessionAccount.getId() == id ? sessionAccount : accountService.getById(id);
        account.setRegistrationDate(oldAccount.getRegistrationDate());
        if (account.getImage().length == 0) {
            byte[] oldImage = oldAccount.getImage();
            if (oldImage != null) {
                insertImage(account, oldImage);
            } else {
                account.setImage(null);
            }
        }
        try {
            accountService.updateAccount(account);
            writeToSession(session, oldAccount);
            jmsTemplate.convertAndSend(ORDER_QUEUE, new JmsMessage(account.getEmail(), "Your account is updated"));
        } catch (Exception e) {
            return ("redirect:/updateAccount?id=" + id + "&error=Phone number is exist, try another");
        }
        return "redirect:/account?id=" + id;
    }

    @RequestMapping("/registration")
    public String showRegistration() {
        return "registrationView";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String doRegistration(@ModelAttribute Account account, HttpSession session, HttpServletRequest req) {
        String email = account.getEmail();
        String password = account.getPassword();
        logger.info("Register a new account with email='{}'", email);
        removeNullPhones(account);
        if (account.getImage().length == 0) {
            account.setImage(null);
        }
        if (accountService.getByEmail(email) == null) {
            try {
                account.setPassword(getEncodePassword(password));
                accountService.createAccount(account);
                req.login(email, password);
                writeToSession(session, account);
                jmsTemplate.convertAndSend(ORDER_QUEUE, new JmsMessage(account.getEmail(), "Your are successfully signed up"));
                return "redirect:/account?id=" + account.getId();
            } catch (Exception e) {
                logger.debug("OOPS - " + e.getMessage());
                req.setAttribute("error", "Phone number is exist, try another");
                return "registrationView";
            }
        } else {
            req.setAttribute("error", "Email is exists, try another");
            return "registrationView";
        }
    }

    private void writeToSession(HttpSession session, Account account) {
        session.setAttribute("account", account);
    }

    private void removeNullPhones(Account account) {
        List<Phone> phones = account.getPhones();
        if (phones != null) {
            phones.removeIf(phone -> phone.getNumber() == null);
        }
    }

    private void insertImage(Account account, byte[] image) {
        if (image.length > MIN_IMAGE_SIZE && image.length < MAX_IMAGE_SIZE) {
            account.setImage(image);
        }
    }

}
