package com.getjavajob.training.socialnetwork.ui;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.GroupService;
import com.getjavajob.training.socialnetwork.ui.utils.JsonMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Controller
public class MessageController {

    private static final Logger logger = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "/sendPost", method = RequestMethod.POST)
    public String sendPost(@RequestParam(value = "accountId", required = false) Long accountId,
                           @RequestParam(value = "groupId", required = false) Long groupId,
                           @RequestParam(value = "text") String text,
                           @SessionAttribute("account") Account sender) {
        long senderId = sender.getId();

        if (accountId != null) {
            logger.info("Send post from account with id='{}' to account wall with id='{}'", senderId, accountId);
            accountService.sendPost(sender, accountId, text);
            return "redirect:/account?id=" + accountId;
        } else {
            logger.info("Send post from account with id='{}' to group wall with id='{}'", senderId, groupId);
            groupService.sendGroupPost(sender, groupId, text);
            return "redirect:/group?id=" + groupId;
        }
    }

    @RequestMapping(value = "/deleteMessage")
    public String deleteMessage(@RequestParam(value = "accMsgId", required = false) Long accMsgId,
                                @RequestParam(value = "groupMsgId", required = false) Long groupMsgId,
                                @RequestParam(value = "accountId", required = false) Long accountId,
                                @RequestParam(value = "groupId", required = false) Long groupId) {
        if (accMsgId != null) {
            logger.info("Delete post with id='{}' from account wall with id='{}'", accMsgId, accountId);
            accountService.deleteMessage(accMsgId);
            return "redirect:/account?id=" + accountId;
        } else {
            logger.info("Delete post with id='{}' from group wall with id='{}'", groupMsgId, groupId);
            groupService.deleteGroupPost(groupMsgId);
            return "redirect:/group?id=" + groupId;
        }
    }

    @RequestMapping(value = "/chats", method = RequestMethod.GET)
    public ModelAndView getChats(@SessionAttribute Account account,
                                 @RequestParam(required = false) Long recipientId) {
        long id = account.getId();
        logger.info("Show all interlocutors of the account with id='{}'", id);
        ModelAndView modelAndView = new ModelAndView("messagesView");
        List<Account> interlocutors = accountService.getInterlocutors(id);
        if (interlocutors.isEmpty() && recipientId == null) {
            return modelAndView.addObject("noChats", true);
        } else {
            Account recipient;
            if (recipientId != null) {
                recipient = accountService.getById(recipientId);
                if (!interlocutors.contains(recipient)) {
                    interlocutors.add(recipient);
                }
            } else {
                recipient = interlocutors.get(0);
            }
            modelAndView.addObject("recipient", recipient);
            modelAndView.addObject("dialog", accountService.getChat(account, recipient));
        }
        modelAndView.addObject("interlocutors", interlocutors);
        return modelAndView;
    }

    @MessageMapping("/chat/{sender}/{recipient}")
    @SendTo("/topic/{sender}/{recipient}")
    public JsonMessage getPersonalMessage(JsonMessage message, SimpMessageHeaderAccessor headerAccessor) {
        Account sender = (Account) Objects.requireNonNull(headerAccessor.getSessionAttributes()).get("account");
        logger.info("Account with id='{}' send personal message to account with id='{}'", sender.getId(), message.getRecipientId());
        accountService.sendPersonalMessage(sender, Long.valueOf(message.getRecipientId()), message.getMsgTxt());
        String date = new SimpleDateFormat("HH:mm:ss dd:MM:yyyy").format(new Date());
        return new JsonMessage(message.getSenderId(), message.getRecipientId(), message.getMsgTxt(), date);
    }

}
