package com.getjavajob.training.socialnetwork.ui;

import com.getjavajob.training.mailer.web.JmsMessage;
import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

import static com.getjavajob.training.socialnetwork.Application.ORDER_QUEUE;

@Controller
public class RelationshipController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private JmsTemplate jmsTemplate;

    @RequestMapping(value = "/friends", method = RequestMethod.GET)
    public ModelAndView friends(@RequestParam("id") long id) {
        logger.info("Loading all friends of the account with id='{}'", id);
        ModelAndView modelAndView = new ModelAndView("friendsView");
        Map<String, List<Account>> allRelations = accountService.getFriends(id);
        modelAndView.addObject("friends", allRelations.get("friends"));
        modelAndView.addObject("requestsFrom", allRelations.get("requestsFromMe"));
        modelAndView.addObject("requestsTo", allRelations.get("requestsToMe"));
        modelAndView.addObject("account", accountService.getById(id));
        return modelAndView;
    }

    @RequestMapping(value = "/addFriend", method = RequestMethod.POST)
    public String addFriend(@RequestParam long friendId, @SessionAttribute("account") Account account,
                            @RequestParam(required = false) Boolean isPage) {
        logger.info("Accepting friend request by accounts from id='{}' to id='{}'", friendId, account.getId());
        Account friend = accountService.getById(friendId);
        accountService.acceptFriendRequest(account, friend);
        jmsTemplate.convertAndSend(ORDER_QUEUE, new JmsMessage(friend.getEmail(), account.getName() + " " +
                account.getSurname() + " accepted friend request"));
        return isPage == null ? "redirect:/friends?id=" + account.getId() : "redirect:/account?id=" + friendId;
    }

    @RequestMapping(value = "/friendRequest", method = RequestMethod.POST)
    public String sendFriendRequest(@RequestParam long friendId, @SessionAttribute("account") Account account) {
        logger.info("Sending friend request by accounts from id='{}' to id='{}'", account.getId(), friendId);
        Account friend = accountService.getById(friendId);
        accountService.sendFriendRequest(account, friend);
        jmsTemplate.convertAndSend(ORDER_QUEUE, new JmsMessage(friend.getEmail(), account.getName() + " " +
                account.getSurname() + " send friend request"));
        return "redirect:/account?id=" + friendId;
    }

    @RequestMapping(value = "/deleteFriend", method = RequestMethod.POST)
    public String deleteFriend(@RequestParam long friendId, @SessionAttribute("account") Account account,
                               @RequestParam(required = false) Boolean isPage) {
        logger.info("Delete friend with id='{}' from account with id='{}'", friendId, account.getId());
        accountService.deleteRelationship(account.getId(), friendId);
        return isPage == null ? "redirect:/friends?id=" + account.getId() : "redirect:/account?id=" + friendId;
    }

    //    Group membership
    @RequestMapping(value = "/groupMembers", method = RequestMethod.GET)
    public ModelAndView showGroupMembers(@RequestParam("groupId") long groupId, @SessionAttribute("account") Account sessionAccount) {
        logger.info("Loading members of the group with id='{}'", groupId);
        ModelAndView modelAndView = new ModelAndView("groupMembersView");
        Map<String, List<Account>> allAccounts = groupService.getGroupMembers(groupId);
        modelAndView.addObject("members", allAccounts.get("members"));
        modelAndView.addObject("requests", allAccounts.get("requests"));
        modelAndView.addObject("moderators", allAccounts.get("moderators"));
        modelAndView.addObject("groupId", groupId);
        modelAndView.addObject("isModerator", groupService.isModerator(sessionAccount.getId(), groupId));
        return modelAndView;
    }

    @RequestMapping(value = "/groups", method = RequestMethod.GET)
    public ModelAndView showGroups(@RequestParam long id) {
        logger.info("Loading groups of the account with id='{}'", id);
        ModelAndView modelAndView = new ModelAndView("groupsView");
        Map<String, List<Group>> allGroups = groupService.getAccountGroups(id);
        modelAndView.addObject("groups", allGroups.get("groups"));
        modelAndView.addObject("requests", allGroups.get("requests"));
        modelAndView.addObject("account", accountService.getById(id));
        return modelAndView;
    }

    @RequestMapping(value = "/memberRequest", method = RequestMethod.POST)
    public String sendGroupMemberRequest(@RequestParam long groupId, @SessionAttribute("account") Account account) {
        logger.info("Sending member request from account with id='{}' to the group with id='{}'", account.getId(), groupId);
        groupService.addGroupMember(account, groupService.getById(groupId));
        return "redirect:/group?id=" + groupId;
    }

    @RequestMapping(value = "/addMember", method = RequestMethod.POST)
    public String addGroupMember(@RequestParam long id, @RequestParam long groupId) {
        logger.info("Accepting member request from account with id='{}' to the group with id='{}'", id, groupId);
        groupService.acceptGroupMember(id, groupId);
        return "redirect:/groupMembers?groupId=" + groupId;
    }

    @RequestMapping(value = "/deleteMember", method = RequestMethod.POST)
    public String deleteMember(@RequestParam long id, @RequestParam long groupId) {
        logger.info("Delete member with id='{}' from the group with id='{}'", id, groupId);
        groupService.deleteGroupMember(id, groupId);
        return "redirect:/groupMembers?groupId=" + groupId;
    }

    @RequestMapping(value = "/makeModerator", method = RequestMethod.POST)
    public String makeModerator(@RequestParam long id, @RequestParam long groupId) {
        logger.info("Making member with id='{}' as moderator of the group with id='{}'", id, groupId);
        groupService.makeModerator(id, groupId);
        return "redirect:/groupMembers?groupId=" + groupId;
    }

    @RequestMapping(value = "/quitGroup", method = RequestMethod.POST)
    public String quitGroup(@RequestParam long groupId, @SessionAttribute("account") Account account) {
        logger.info("Checking out member with id='{}' from the group with id='{}'", account.getId(), groupId);
        groupService.deleteGroupMember(account.getId(), groupId);
        return "redirect:/group?id=" + groupId;
    }

}
