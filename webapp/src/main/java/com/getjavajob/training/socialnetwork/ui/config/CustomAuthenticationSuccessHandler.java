package com.getjavajob.training.socialnetwork.ui.config;

import com.getjavajob.training.socialnetwork.common.Account;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

import static com.getjavajob.training.socialnetwork.ui.utils.SecurityUtils.getCurrentUser;
import static java.util.Objects.nonNull;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException {
        Account account = Objects.requireNonNull(getCurrentUser()).getAccount();
        response.setStatus(HttpServletResponse.SC_OK);
        if (nonNull(account)) {
            request.getSession().setAttribute("account", account);
            response.sendRedirect(request.getContextPath() + "/account?id=" + account.getId());
        }
    }

}
