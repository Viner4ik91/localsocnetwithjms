<%@include file="common/header.jsp" %>
<%@include file="common/navbar.jsp" %>

<div class="container">
    <c:if test="${requestScope.account.id != sessionScope.account.id}">
        <p>${requestScope.account.name} ${requestScope.account.surname} friends:</p>
    </c:if>
    <div class="row">
        <div class="col-md-4">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th colspan="3">Friends</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="friend" items="${friends}">
                    <tr>
                        <td style="width: 30%">
                            <c:choose>
                                <c:when test="${friend.image != null}">
                                    <img src="${pageContext.request.contextPath}/printImage?id=${friend.id}"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/resources/picture/default.jpg"
                                         class="card-img-top"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td style="width: 60%; word-break: break-all;">
                            <a href="${pageContext.request.contextPath}/account?id=${friend.id}">
                                    <c:out value="${friend.name} ${friend.surname}"/>
                        </td>
                        <td style="width: 10%">
                            <c:if test="${sessionScope.account.id == requestScope.account.id}">
                                <form action="${pageContext.request.contextPath}/deleteFriend?friendId=${friend.id}"
                                      method="POST">
                                    <button class="btn btn-danger" type="submit">
                                        <span>Delete</span>
                                    </button>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th colspan="3">My requests</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="friendRequest" items="${requestsFrom}">
                    <tr>
                        <td style="width: 30%">
                            <c:choose>
                                <c:when test="${friendRequest.image != null}">
                                    <img src="${pageContext.request.contextPath}/printImage?id=${friendRequest.id}"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/resources/picture/default.jpg"
                                         class="card-img-top"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td style="width: 60%; word-break: break-all;">
                            <a href="${pageContext.request.contextPath}/account?id=${friendRequest.id}">
                                    <c:out value="${friendRequest.name} ${friendRequest.surname}"/>
                        </td>
                        <td style="width: 10%">
                            <c:if test="${sessionScope.account.id == requestScope.account.id}">
                                <form action="${pageContext.request.contextPath}/deleteFriend?friendId=${friendRequest.id}"
                                      method="POST">
                                    <button class="btn btn-danger" type="submit">
                                        <span>Remove</span>
                                    </button>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th colspan="3">Requests to me</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="friendRequest" items="${requestScope.requestsTo}">
                    <tr>
                        <td style="width: 30%">
                            <c:choose>
                                <c:when test="${friendRequest.image != null}">
                                    <img src="${pageContext.request.contextPath}/printImage?id=${friendRequest.id}"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/resources/picture/default.jpg"
                                         class="card-img-top"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td style="width: 60%; word-break: break-all;">
                            <a href="${pageContext.request.contextPath}/account?id=${friendRequest.id}">
                                    <c:out value="${friendRequest.name} ${friendRequest.surname}"/>
                        </td>
                        <td style="width: 10%">
                            <c:if test="${sessionScope.account.id == requestScope.account.id}">
                                <form action="${pageContext.request.contextPath}/addFriend?friendId=${friendRequest.id}"
                                      method="POST">
                                    <button class="btn btn-success" type="submit">
                                        <span>Add</span>
                                    </button>
                                </form>
                                <form action="${pageContext.request.contextPath}/deleteFriend?friendId=${friendRequest.id}"
                                      method="POST">
                                    <button class="btn btn-danger" type="submit">
                                        <span>Remove</span>
                                    </button>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<%@include file="common/footer.jsp" %>
