<%@include file="common/header.jsp" %>
<%@include file="common/navbar.jsp" %>

<div class="container" align="center">
    <h1>${requestScope.group.id == 0 ? 'Creation' : 'Edit'}</h1>
    <div>
        <c:if test="${requestScope.group.id == 0}">
        <form action="<c:url value="/createGroup"/>" method="post">
            <input type="hidden" name="creator.id" value="${sessionScope.account.id}"/>
            </c:if>
            <c:if test="${requestScope.group.id != 0}">
            <form action="<c:url value="/updateGroup?id=${requestScope.group.id}"/>" method="post"
                  enctype="multipart/form-data">
                <input type="hidden" name="creator.id" value="${requestScope.group.creator.id}"/>
                </c:if>

                <table>
                    <tr>
                        <td><input type="text" name="groupName" value='${requestScope.group != null ?
                requestScope.group.groupName : ""}' placeholder="Group name"></td>
                    </tr>
                    <tr>
                        <td><input type="text" name="description" value='${requestScope.group != null ?
                requestScope.group.description : ""}' placeholder="Description"></td>
                    </tr>
                </table>
                <button class="btn btn-primary">${requestScope.group.id == 0 ? 'Continue registration' : 'Updating'}</button>
            </form>
    </div>
</div>
<%@include file="common/footer.jsp" %>