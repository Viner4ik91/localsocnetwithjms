<%@include file="common/header.jsp" %>
<%@include file="common/navbar.jsp" %>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card" style="width: 18rem;">
                <c:choose>
                    <c:when test="${requestScope.group.getImage() != null}">
                        <img src="printImage?groupId=${requestScope.group.getId()}" class="card-img-top">
                    </c:when>
                    <c:otherwise>
                        <img src="../../resources/picture/default.jpg" class="card-img-top">
                    </c:otherwise>
                </c:choose>
                <c:if test="${isModerator}">
                    <form action="${pageContext.request.contextPath}/updateImage?groupId=${requestScope.group.id}"
                          method="post" enctype="multipart/form-data">
                        <div class="form-group input-group">
                            <div class="custom-file">
                                <input name="image" type="file" class="custom-file-input" id="inputGroupFile04"
                                       aria-describedby="inputGroupFileAddon04">
                                <label class="custom-file-label" for="inputGroupFile04">Choose image</label>
                            </div>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </form>
                </c:if>
                <c:choose>
                    <c:when test="${!isMember}">
                        <form action="${pageContext.request.contextPath}/memberRequest?groupId=${requestScope.group.id}"
                              method="POST">
                            <button class="btn btn-success btn-block" type="submit">
                                <span>Join</span>
                            </button>
                        </form>
                    </c:when>
                    <c:otherwise>
                        <form action="${pageContext.request.contextPath}/quitGroup?groupId=${requestScope.group.id}"
                              method="POST">
                            <button class="btn btn-danger btn-block" type="submit">
                                <span>Quit</span>
                            </button>
                        </form>
                    </c:otherwise>
                </c:choose>
                <a href="${pageContext.request.contextPath}/groupMembers?groupId=${requestScope.group.id}">Members</a>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-4">
                    <div class="card-body">
                        <h5 class="card-title">${requestScope.group.groupName}</h5>
                    </div>
                </div>
                <div class="col-md-4 offset-4">
                    <c:if test="${isModerator}">
                        <a class="btn btn-primary" href="updateGroup?id=${requestScope.group.id}">Update group</a>
                    </c:if>
                </div>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Description: ${requestScope.group.description}</li>
                <li class="list-group-item">Creator: ${group.creator.name} ${group.creator.surname}</li>
                <li class="list-group-item font-italic">Registration
                    date: ${requestScope.group.creationDate}</li>
            </ul>
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content profile-tab">
                        <hr>
                        <h6>
                            What is new?
                        </h6>
                        <form action="${pageContext.request.contextPath}/sendPost?groupId=${requestScope.group.id}"
                              method="POST">
                            <div class="form-group">
                                <textarea class="form-control" rows="5" type="text" name="text"
                                          style="resize:none"></textarea>
                                <input class="btn btn-primary" type="submit" value="Send message" style="float: right">
                            </div>
                        </form>
                        <br>
                        <table class="table table-hover">
                            <tbody>
                            <c:forEach var="message" items="${wallMessages}">
                                <tr>
                                    <td style="width: 10%">
                                        <a href="${pageContext.request.contextPath}/account?id=${message.sender.id}">
                                            <c:out value="${message.sender.name}"/></a>
                                    </td>
                                    <td style="width: 70%; word-break: break-all;">
                                        <c:out value="${message.msgTxt}"/>
                                    </td>
                                    <td style="width: 30%">
                                        <small>
                                            <c:out value="${message.creationDate.toLocalDate()}"/>
                                        </small>
                                    </td>
                                    <td style="width: 1%">
                                        <c:if test="${isModerator}">
                                            <small>
                                                <a href="deleteMessage?groupMsgId=${message.id}&groupId=${requestScope.group.id}"
                                                   class="badge badge-pill badge-danger">x</a>
                                            </small>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="common/footer.jsp" %>
