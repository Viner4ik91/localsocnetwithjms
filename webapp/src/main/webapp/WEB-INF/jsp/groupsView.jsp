<%@include file="common/header.jsp" %>
<%@include file="common/navbar.jsp" %>

<div class="container">
    <c:if test="${requestScope.account.id != sessionScope.account.id}">
        <p>${requestScope.account.name} ${requestScope.account.surname} groups:</p>
    </c:if>
    <div class="row">
        <div class="col-md-6">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th colspan="3">Groups</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="group" items="${groups}">
                    <tr>
                        <td style="width: 30%">
                            <c:choose>
                                <c:when test="${group.image != null}">
                                    <img src="${pageContext.request.contextPath}/printImage?groupId=${group.id}"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/resources/picture/default.jpg"
                                         class="card-img-top"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td style="width: 60%; word-break: break-all;">
                            <a href="${pageContext.request.contextPath}/group?id=${group.id}">
                                    <c:out value="${group.groupName}"/>
                        </td>
                        <td style="width: 10%">
                            <c:if test="${account.id == sessionScope.account.id || sessionScope.account.admin}">
                                <form action="${pageContext.request.contextPath}/deleteMember?id=${account.id}&groupId=${group.id}"
                                      method="POST">
                                    <button class="btn btn-danger" type="submit">
                                        <span>Out</span>
                                    </button>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th colspan="3">Requests</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="request" items="${requests}">
                    <tr>
                        <td style="width: 30%">
                            <c:choose>
                                <c:when test="${request.image != null}">
                                    <img src="${pageContext.request.contextPath}/printImage?groupId=${request.id}"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/resources/picture/default.jpg"
                                         class="card-img-top"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td style="width: 60%; word-break: break-all;">
                            <a href="${pageContext.request.contextPath}/group?id=${request.id}">
                                    <c:out value="${request.groupName}"/>
                        </td>
                        <td style="width: 10%">
                            <c:if test="${account.id == sessionScope.account.id || sessionScope.account.admin}">
                                <form action="${pageContext.request.contextPath}/deleteMember?id=${account.id}&groupId=${request.id}"
                                      method="POST">
                                    <button class="btn btn-danger" type="submit">
                                        <span>Delete request</span>
                                    </button>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<%@include file="common/footer.jsp" %>
