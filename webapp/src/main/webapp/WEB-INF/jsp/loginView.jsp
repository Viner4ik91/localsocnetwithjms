<%@include file="common/header.jsp" %>

<div class="container" align="center">
    <form class="form-signin">
        <c:choose>
            <c:when test='${requestScope.error != null}'>
                <div class="alert alert-danger">
                    <c:out value='${requestScope.error}'/>
                </div>
            </c:when>
            <c:when test='${requestScope.logout != null}'>
                <div class="alert alert-danger">
                    <c:out value='${requestScope.logout}'/>
                </div>
            </c:when>
        </c:choose>
    </form>
    <form class="form-signin" action="${pageContext.request.contextPath}/login" method="post">
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="username" id="inputEmail" class="form-control" placeholder="Email address" required
               autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password"
               required>
        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" name="remember-me"> Remember me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
    </form>
    <form class="form-signin" action="${pageContext.request.contextPath}/registration" method="get">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Registration</button>
    </form>
</div>
