<%@include file="common/header.jsp" %>
<%@include file="common/navbar.jsp" %>

<script src="${pageContext.request.contextPath}/resources/js/webSocketChat.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.2.0/sockjs.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.js"></script>
<div class="container">
    <div class="row">
        <h1>My dialogs</h1>
    </div>
    <c:if test="${noChats}">
        <p>No chats</p>
    </c:if>
    <div hidden id="senderId">${sessionScope.account.id}</div>
    <div hidden id="recipientId">${recipient.id}</div>
    <div class="row">
        <div class="col-md-3">
            <table class="table table-hover">
                <tbody>
                <c:forEach var="account" items="${interlocutors}">
                    <tr>
                        <td style="width: 30%">
                            <c:choose>
                                <c:when test="${account.image != null}">
                                    <img src="${pageContext.request.contextPath}/printImage?id=${account.id}"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/resources/picture/default.jpg"
                                         class="card-img-top"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <a class="messageAccount btn btn-secondary ${account.id == recipient.id ? "active" : ""}"
                               name="${account.id}"
                               href="${pageContext.request.contextPath}/chats?recipientId=${account.id}">${account.name}
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <div class="col-md-6">
            <div id="messages">
                <c:forEach var="message" items="${dialog}">
                    <c:choose>
                        <c:when test='${message.sender.id == sessionScope.account.id}'>
                            <div class="row">
                                <div class="col-auto mr-auto"></div>
                                <div class="alert alert-primary float-right">
                                    <p>${message.msgTxt}</p>
                                    <small class="form-text text-muted">
                                            ${message.creationDate.toLocalDate()}
                                    </small>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="row">
                                <div class="col-auto"></div>
                                <div class="alert alert-dark float-left">
                                    <p>${message.msgTxt}</p>
                                    <small class="form-text text-muted">
                                            ${message.creationDate.toLocalDate()}
                                    </small>
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </div>

            <div class="form-group">
                <label for="textMessage"></label>
                <textarea id="textMessage" class="form-control" name="message" rows="4" required></textarea>
            </div>
            <a id="sendMsg" class="btn btn-primary disabled">Send message</a>
        </div>
    </div>
</div>
<%@include file="common/footer.jsp" %>
