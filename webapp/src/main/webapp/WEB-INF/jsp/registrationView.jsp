<%@include file="common/header.jsp" %>
<%@include file="common/navbar.jsp" %>
<script src="${pageContext.request.contextPath}/resources/js/phoneEditor.js"></script>

<div class="container">
    <div class="card bg-light">
        <h4 class="card-title mt-3 text-center">Registration</h4>
        <article class="card-body mx-auto" style="max-width: 400px;">
            <form class="form-signin">
                <c:choose>
                    <c:when test='${requestScope.error != null}'>
                        <div class="alert alert-danger">
                            <c:out value='${requestScope.error}'/>
                        </div>
                    </c:when>
                </c:choose>
            </form>
            <form action="${pageContext.request.contextPath}/registration" method="post"
                  enctype="multipart/form-data">
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="surname" class="form-control" placeholder="Fisrt name" type="text" required/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="name" class="form-control" placeholder="Last name" type="text" required/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="middlename" class="form-control" placeholder="Middle name" type="text"/>
                </div>
                <!-- form-group// -->
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                    </div>
                    <input name="email" class="form-control" placeholder="Email address" type="email" required/>
                </div> <!-- form-group// -->
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-birthday-cake"></i> </span>
                    </div>
                    <input name="birthday" class="form-control" placeholder="Birthday" type="date" required/>
                </div> <!-- form-group// -->
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-home"></i> </span>
                    </div>
                    <input name="homeAddress" class="form-control" placeholder="Home address" type="text">
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-briefcase"></i> </span>
                    </div>
                    <input name="workAddress" class="form-control" placeholder="Work address" type="text">
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-comment"></i> </span>
                    </div>
                    <input name="icq" class="form-control" placeholder="ICQ" type="text">
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fab fa-skype"></i> </span>
                    </div>
                    <input name="skype" class="form-control" placeholder="Skype" type="text">
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-question"></i> </span>
                    </div>
                    <textarea name="additionalInfo" class="form-control" placeholder="Additional information"
                              rows="3"></textarea>
                </div>
                <div class="phones"></div>
                <div class="newPhone">
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                        </div>
                        <select class="btn btn-primary" id="phoneType">
                            <option selected value="person">Person</option>
                            <option value="work">Work</option>
                        </select>
                        <input class="form-control" placeholder="Person phone" type="tel" id="phoneNumber"
                               title="For example: +79165556633"/>
                        <div class="input-group-btn">
                            <button class="btn btn-success" id="btn-add">
                                <span>Add</span>
                            </button>
                        </div>
                    </div>
                </div>
                <!--newPhone-group-->
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                    </div>
                    <input name="password" class="form-control" placeholder="Create password" type="password" required/>
                </div> <!-- form-group// -->
                <div class="form-group input-group">
                    <div class="custom-file">
                        <input name="image" type="file" class="custom-file-input" id="inputGroupFile04"
                               aria-describedby="inputGroupFileAddon04">
                        <label class="custom-file-label" for="inputGroupFile04">Choose image</label>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"
                            onclick="return confirm('Are you sure you want create new Account?')"> Create Account
                    </button>
                </div> <!-- form-group// -->
                <p class="text-center">Have an account? <a href="${pageContext.request.contextPath}/login">Log
                    In</a>
                </p>
            </form>
        </article>
    </div> <!-- card.// -->

</div>
<!--container end.//-->
<%@include file="common/footer.jsp" %>