<%@include file="common/header.jsp" %>
<%@include file="common/navbar.jsp" %>
<script src="${pageContext.request.contextPath}/resources/js/searchByPage.js"></script>
<div class="container">
    <div class="card bg-light mi">
        <h4 class="card-title mt-3 text-center">Result of ${requestScope.value}:</h4>
        <div class="row" style="height: 350px;">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <input type="hidden" id="pattern" value="${requestScope.value}"/>
                <input type="hidden" id="accountQty" value="${requestScope.accountQty}"/>
                <input type="hidden" id="groupQty" value="${requestScope.groupQty}"/>
                <ul class="nav nav-tabs">
                    <li class="nav-item active"><a class="nav-link active" data-toggle="tab"
                                                   href="#accounts">Accounts</a></li>
                    <li class="nav-item "><a class="nav-link" data-toggle="tab" href="#groups">Groups</a></li>
                </ul>
                <div class="tab-content">
                    <div id="accounts" class="tab-pane fade in active show">
                        <table class="table table-hover">
                            <tbody id="accountResult">
                            </tbody>
                        </table>
                        <button id="backAcc" class="btn btn-primary" style="float:left">
                            <span class="fas fa-arrow-left"></span>
                        </button>
                        <button id="nextAcc" class="btn btn-primary" style="float:right">
                            <span class="fas fa-arrow-right"></span>
                        </button>
                    </div>
                    <div id="groups" class="tab-pane fade">
                        <table class="table table-hover">
                            <tbody id="groupsResult">
                            </tbody>
                        </table>
                        <button id="backGroup" class="btn btn-primary" style="float:left">
                            <span class="fas fa-arrow-left"></span>
                        </button>
                        <button id="nextGroup" class="btn btn-primary" style="float:right">
                            <span class="fas fa-arrow-right"></span>
                        </button>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div> <!-- card.// -->
</div>
<!--container end.//-->
<div class="modal"><!-- Place at bottom of page --></div>
<%@include file="common/footer.jsp" %>