<%@include file="common/header.jsp" %>
<%@include file="common/navbar.jsp" %>
<script src="${pageContext.request.contextPath}/resources/js/phoneEditor.js"></script>

<div class="container">
    <div class="card bg-light">
        <c:if test='${requestScope.error != null}'>
            <div class="alert alert-danger text-center">
                <c:out value='${requestScope.error}'/>
            </div>
        </c:if>
        <h4 class="card-title mt-3 text-center">Update Account</h4>
        <form action="fromXml?id=${account.id}" method="POST" class="text-center"
              enctype="multipart/form-data">
            <input name="file" type="file">
            <button type="submit">Upload from XML</button>
        </form>
        <article class="card-body mx-auto">
            <form action="${pageContext.request.contextPath}/updateAccount?id=${account.id}"
                  method="post"
                  enctype="multipart/form-data">
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="surname" class="form-control" placeholder="First name" type="text"
                           value="${account.surname}" required/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="name" class="form-control" placeholder="Last name" type="text"
                           value="${account.name}" required/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="middlename" class="form-control" placeholder="Middle name" type="text"
                           value="${account.middlename}"/>
                </div>
                <!-- form-group// -->
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                    </div>
                    <input name="email" class="form-control" placeholder="Email address" type="email"
                           value="${account.email}" required/>
                </div> <!-- form-group// -->
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-birthday-cake"></i> </span>
                    </div>
                    <input name="birthday" class="form-control" placeholder="Birthday" type="date"
                           value="${account.birthday}" required/>
                </div> <!-- form-group// -->
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-home"></i> </span>
                    </div>
                    <input name="homeAddress" class="form-control" placeholder="Home address" type="text"
                           value="${account.homeAddress}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-briefcase"></i> </span>
                    </div>
                    <input name="workAddress" class="form-control" placeholder="Work address" type="text"
                           value="${account.workAddress}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-comment"></i> </span>
                    </div>
                    <input name="icq" class="form-control" placeholder="ICQ" type="text"
                           value="${account.icq}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fab fa-skype"></i> </span>
                    </div>
                    <input name="skype" class="form-control" placeholder="Skype" type="text"
                           value="${account.skype}"/>
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-question"></i> </span>
                    </div>
                    <textarea name="additionalInfo" class="form-control" placeholder="Additional information"
                              rows="3">${account.additionalInfo}</textarea>
                </div>
                <div class="phones" id="phones">
                    <c:forEach var="phone" items="${account.phones}" varStatus="i">
                        <div class="form-group input-group" id="addedPhone">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                            </div>
                            <input type="hidden" name="phones[${i.index}].id" value="${phone.id}"/>
                            <input type="hidden" name="phones[${i.index}].accountId" value="${phone.accountId}"/>
                            <select class="btn btn-primary" name="phones[${i.index}].type">
                                <option value="person" ${phone.type=='person' ? 'selected' : ''}>Person</option>
                                <option value="work" ${phone.type=='work' ? 'selected' : ''}>Work</option>
                            </select>
                            <input class="form-control" type="tel" name="phones[${i.index}].number"
                                   value="${phone.number}" readonly/>
                            <div class="input-group-btn">
                                <button class="btn btn-danger btn-remove-phone">
                                    <span>-</span>
                                </button>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <!-- oldPhone-group -->

                <div class="newPhone">
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                        </div>
                        <select class="btn btn-primary" id="phoneType">
                            <option selected value="person">Person</option>
                            <option value="work">Work</option>
                        </select>
                        <input class="form-control" placeholder="Person phone" type="tel" id="phoneNumber"
                               title="For example: +79165556633"/>
                        <div class="input-group-btn">
                            <button class="btn btn-success" id="btn-add">
                                <span>Add</span>
                            </button>
                        </div>
                    </div>
                </div>
                <!--newPhone-group-->
                <div class="form-group input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                    </div>
                    <input name="password" class="form-control" placeholder="Create password" type="password" required/>
                </div> <!-- form-group// -->
                <div class="form-group input-group">
                    <div class="custom-file">
                        <input name="image" type="file" class="custom-file-input" id="image"
                               aria-describedby="inputGroupFileAddon04">
                        <label class="custom-file-label" for="image">Choose image</label>
                    </div>
                </div>
                <input type="hidden" name="oldImage" value="${account.image}"/>
                <input type="hidden" name="admin" value="${account.admin}"/>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"
                            onclick="return confirm('Are you sure you want to save the changes?')"> Update
                    </button>
                </div> <!-- form-group// -->
            </form>
        </article>
    </div> <!-- card.// -->

</div>
<!--container end.//-->
<%@include file="common/footer.jsp" %>
