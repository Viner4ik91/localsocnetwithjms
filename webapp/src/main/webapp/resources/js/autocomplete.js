$(function () {
    $("#search-ajax").autocomplete({
        source: function (request, response) {
            $.get(context + '/searchAjax?search=' + request.term, function (data) {
                response($.map(data, function (object) {
                    return object.hasOwnProperty("surname") ?
                        {label: object.name + ' ' + object.surname + ' :account', value: object.id, type: "account"} :
                        {label: object.groupName + ' :group', value: object.id, type: "group"};
                }));
            });
        },
        minLength: 2,
        select: function (event, ui) {
            let id = ui.item.value;
            location.href = ui.item.type === "account" ? context + '/account?id=' + id : context + '/group?id=' + id;
            return false;
        }
    });
});

