$(function () {

    $('.phones').on('click', '.btn-remove-phone', function (e) {
        e.preventDefault();
        $(this).closest('div #addedPhone').remove();
    });

    var count = $('div #addedPhone').length;
    $('#btn-add').click(function (e) {
        e.preventDefault();
        const newPhone = $('.newPhone');
        $(newPhone).find('.incorrectNum').remove();
        const number = newPhone.find('#phoneNumber').val();
        const type = newPhone.find('#phoneType').val();
        if (!/^(8|\+7)\d{10}$/.test(number)) {
            newPhone.append('<div class="alert alert-danger incorrectNum">\n' +
                '     <p>Incorrect number! Try again in expected form</p>\n' +
                '</div>');
        } else {
            $('.phones').append(
                '<div class="form-group input-group" id="addedPhone">\n' +
                '        <div class="input-group-prepend">\n' +
                '                 <span class="input-group-text"> <i class="fa fa-phone"></i> </span>\n' +
                '        </div>\n' +
                '                 <select class="btn btn-primary" name="phones[' + count + '].type">\n' +
                '                     <option value="person" ' + (type === "person" ? "selected" : "") + '>Person</option>\n' +
                '                     <option value="work" ' + (type === "work" ? "selected" : "") + '>Work</option>\n' +
                '                 </select>\n' +
                '                     <input class="form-control" type="tel" name="phones[' + count + '].number" value="' + number + '"\n' +
                '                         readonly/>\n' +
                '                 <div class="input-group-btn">\n' +
                '                     <button class="btn btn-danger btn-remove-phone">\n' +
                '                         <span>-</span>\n' +
                '                     </button>\n' +
                '                 </div>\n' +
                ' </div>');
            $(this).closest(newPhone).find("#phoneNumber").val("");
            count++;
        }
    });

});