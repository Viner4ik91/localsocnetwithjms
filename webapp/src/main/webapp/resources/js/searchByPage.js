body = $("body");
$(document).ajaxStart(function () {
    body.addClass("loading");
}).ajaxStop(function () {
    body.removeClass("loading");
});

let maxResult = 3;
$(function () {
    let backButton = $("#backAcc");
    let nextButton = $("#nextAcc");
    let allAccounts = $("#accountQty").val();
    backButton.hide();
    let pattern = $("#pattern").val();
    let currentPage = 0;
    let searchPageWithMaxResult = context + '/searchByPage?search=' + pattern + '&maxResult=' + maxResult + '&page=';

    $.ajax({
        url: searchPageWithMaxResult + currentPage,
        method: "GET",
        dataType: "json",
        success: function (data) {
            if (data.length < 1) {
                nextButton.hide();
                backButton.hide();
                $("#accountResult").append("<p style='padding-top:10px'>No accounts</p>");
            } else {
                // $("#accounts").addClass("show");
                printAccounts(data);
                checkAllAccountsOnPage()
            }
        }
    });

    nextButton.click(function () {
        $("#accountResult").html("");
        currentPage++;
        backButton.show();
        $.ajax({
            url: searchPageWithMaxResult + currentPage,
            method: "GET",
            dataType: "json",
            success: function (data) {
                printAccounts(data);
                checkAllAccountsOnPage()
            }
        });
    });

    backButton.click(function () {
        $("#accountResult").html("");
        currentPage--;
        nextButton.show();
        if (currentPage <= 0) {
            backButton.hide();
        }
        $.ajax({
            url: searchPageWithMaxResult + currentPage,
            method: "GET",
            dataType: "json",
            success: function (data) {
                printAccounts(data);
            }
        });
    });

    function checkAllAccountsOnPage() {
        if ((currentPage + 1) * maxResult >= allAccounts) {
            nextButton.hide();
        }
    }

    function printAccounts(data) {
        for (var i = 0; i < data.length; ++i) {
            $("#accountResult").append(
                "<tr>" +
                "<td style=\"width: 20%\">" +
                (data[i].image !== null ?
                    "<img class=\'rounded-circle\' src=\"" + context + "/printImage?id=" + data[i].id + "\" " +
                    "style=\"width:56px;height:56px;border:1px solid grey;\"/>" :
                    "" + context + "<img class=\'rounded-circle\' src=" + "" +
                    "\"/resources/picture/default.jpg\" style=\"width:56px;height:56px;border:1px solid grey;\"/>") +
                "</td>" +
                "<td style=\"width: 80%; word-break: break-all;\">" +
                "<a href=\"" + context + "/account?id=" + data[i].id + "\">" + data[i].name + " " + data[i].surname +
                "</a>" +
                "</td>" +
                "</tr>")
        }
    }
});

//For groups
$(function () {
    let backButton = $("#backGroup");
    let nextButton = $("#nextGroup");
    let allGroups = $("#groupQty").val();
    backButton.hide();
    let pattern = $("#pattern").val();
    let currentPage = 0;
    let searchPageWithMaxResult = context + '/searchByPageGroups?search=' + pattern + '&maxResult=' + maxResult + '&page=';

    $.ajax({
        url: searchPageWithMaxResult + currentPage,
        method: "GET",
        dataType: "json",
        success: function (data) {
            if (data.length < 1) {
                nextButton.hide();
                backButton.hide();
                $("#groupsResult").append("<p style='padding-top:10px'>No groups</p>");
            } else {
                printGroups(data);
                checkAllGroupsOnPage()
            }
        }
    });

    nextButton.click(function () {
        $("#groupsResult").html("");
        currentPage++;
        backButton.show();
        $.ajax({
            url: searchPageWithMaxResult + currentPage,
            method: "GET",
            dataType: "json",
            success: function (data) {
                checkAllGroupsOnPage()
                printGroups(data);
            }
        });
    });

    backButton.click(function () {
        $("#groupsResult").html("");
        currentPage--;
        nextButton.show();
        if (currentPage <= 0) {
            backButton.hide();
        }

        $.ajax({
            url: searchPageWithMaxResult + currentPage,
            method: "GET",
            dataType: "json",
            success: function (data) {
                printGroups(data);
            }
        });
    });

    function checkAllGroupsOnPage() {
        if ((currentPage + 1) * maxResult >= allGroups) {
            nextButton.hide();
        }
    }

    function printGroups(data) {
        for (var i = 0; i < data.length; ++i) {
            $("#groupsResult").append(
                "<tr>" +
                "<td style=\"width: 20%\">" +
                (data[i].image !== null ?
                    "<img class=\'rounded-circle\' src=\"" + context + "/printImage?groupId=" + data[i].id + "\" " +
                    "style=\"width:56px;height:56px;border:1px solid grey;\"/>" :
                    "" + context + "<img class=\'rounded-circle\' src=" + "" +
                    "\"/resources/picture/default.jpg\" style=\"width:56px;height:56px;border:1px solid grey;\"/>") +
                "</td>" +
                "<td style=\"width: 80%; word-break: break-all;\">" +
                "<a href=\"" + context + "/group?id=" + data[i].id + "\">" + data[i].groupName +
                "</a>" +
                "</td>" +
                "</tr>")
        }
    }

});