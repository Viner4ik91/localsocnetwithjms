$(function () {
    let client = null;
    let sender = $("#senderId").text();
    let recipient = $("#recipientId").text();
    let text = $("#textMessage");
    let removeOnButton = 'disabled';
    let sendButton = $("#sendMsg");

    function getChat() {
        return sender < recipient ? '/' + sender + '/' + recipient : '/' + recipient + '/' + sender;
    }

    function connect() {
        if (client != null) {
            client.disconnect();
        }
        let socket = new SockJS('/chat');
        client = Stomp.over(socket);
        client.connect({}, function () {
            client.subscribe('/topic' + getChat(), function (message) {
                showMessage(JSON.parse(message.body));
            });
        });
    }

    function getCommon(message) {
        return '<p>' + message.msgTxt + '</p>\n<small class="form-text text-muted">\n' + message.date + '\n' +
            '</small>\n</div>\n</div>'
    }

    function showMessage(message) {
        if (message.senderId === sender) {
            $('#messages').append(
                '<div class="row">\n<div class="col-auto mr-auto"></div>\n' +
                '<div class="alert alert-primary float-right">\n' + getCommon(message)
            );
        } else {
            $('#messages').append(
                '<div class="row">\n<div class="col-auto"></div>\n' +
                '<div class="alert alert-dark float-left">\n' + getCommon(message)
            );
        }
    }

    connect();

    text.keyup(function () {
        if (text.val().length > 0) {
            sendButton.removeClass(removeOnButton);
        } else {
            sendButton.addClass(removeOnButton);
        }
    });

    sendButton.click(function () {
        let message = {
            recipientId: recipient,
            senderId: sender,
            msgTxt: text.val()
        };
        text.val('');
        sendButton.addClass(removeOnButton);
        client.send("/app/chat" + getChat(), {}, JSON.stringify(message))
    });

});